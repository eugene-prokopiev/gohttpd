#!/bin/bash -x

mkdir gohttpd

GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -a -installsuffix cgo -o gohttpd/gohttpd

cd gohttpd
mkdir etc
uuidgen | sed 's/-//g' > etc/machine-id
tar -Jcf ../gohttpd.tar.xz .
cd ..

rm -rf gohttpd
