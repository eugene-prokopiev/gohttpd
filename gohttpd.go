package main

import (
	"log"
	"net"
	"net/http"
	"os"
	"strings"

	"github.com/vishvananda/netlink"
)

func network(address string) {
	ifaces, err := net.Interfaces()
	if err != nil {
		log.Fatal(err.Error())
	}
	for _, iface := range ifaces {
		addrs, err := iface.Addrs()
		if err != nil {
			log.Fatal(err.Error())
		}
		if !strings.Contains(iface.Flags.String(), "loopback") && !strings.Contains(iface.Flags.String(), "up") && addrs == nil {
			link, err := netlink.LinkByName(iface.Name)
			if err != nil {
				log.Fatal(err.Error())
			}
			addr, err := netlink.ParseAddr(address)
			if err != nil {
				log.Fatal(err.Error())
			}
			netlink.AddrAdd(link, addr)
			netlink.LinkSetUp(link)
			break
		}
	}
}

func service(port string) {
	http.Handle("/", http.FileServer(http.Dir("/")))
	http.ListenAndServe(":"+port, nil)
}

func main() {
	args := os.Args
	if len(args) > 2 {
		network(args[1])
		service(args[2])
	} else {
		log.Fatal("No interface address/mask and tcp port in argument list")
	}
}
